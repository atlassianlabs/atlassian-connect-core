module Snap.AtlassianConnect.Instances
    ( ConnectURI(..)
    , getURI
    ) where

import           Control.Monad
import           Data.Aeson.Types
import qualified Data.Text        as T
import           Network.URI

-- | An Atlassian Connect URI
newtype ConnectURI = CURI URI
    deriving(Eq)

-- | Get a regular URI from an Atlassian Connect URI
getURI :: ConnectURI -> URI
getURI (CURI u) = u

instance Show ConnectURI where
    show (CURI u) = show u

instance FromJSON ConnectURI where
   parseJSON (String uriString) = maybe mzero return (fmap CURI . parseURI . T.unpack $ uriString)
   parseJSON _ = mzero
