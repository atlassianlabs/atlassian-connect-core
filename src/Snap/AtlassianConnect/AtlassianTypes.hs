{-# LANGUAGE DeriveGeneric #-}

module Snap.AtlassianConnect.AtlassianTypes
   ( UserKey
   , UserEmail
   , IssueKey
   , IssueId
   , ProjectId
   , ProjectKey
   , IssueSummary
   , UserDetails(..)
   , IssueDetails(..)
   ) where

import qualified Data.ByteString as B
import qualified Data.Text       as T
import           GHC.Generics

-- | Represents an Atlassian User Key. This is guaranteed to be unique on any given tenant.
type UserKey      = T.Text

-- | Represents an Atlassian User Email. This has no uniqueness guarantees; an email does not map to a unique user.
type UserEmail    = B.ByteString -- TODO use a standard email type

-- | The Key of an Atlassian JIRA issue. They are usually in the format <ProjectKey>-<IssueNumber>. Please note
-- that issues can be moved between projects so this is not guaranteed to remain constant.
type IssueKey     = T.Text

-- | The ID of an Atlassian JIRA issue. This id will never change and will always refer to the same issue. It
-- is an immutable reference to a JIRA issue.
type IssueId      = Integer

-- | The user visible key of an Atlassian JIRA Project. Project Keys are not guaranteed to remain the same forever.
type ProjectKey     = T.Text

-- | The project id of an Atlassian JIRA Project. The same project will always have the same project id. The project
-- id is immutable for a given project.
type ProjectId    = Integer

-- | Represents an Atlassian JIRA Issue summary.
type IssueSummary = T.Text

-- | Represents the user details of an Atlassian JIRA user.
data UserDetails = UserDetails
   { userKey   :: UserKey
   , userEmail :: UserEmail
   } deriving (Show, Generic)

-- | Represents simple issue details that every Atlassian JIRA issue will have.
data IssueDetails = IssueDetails
   { issueKey     :: IssueKey
   , issueId      :: IssueId
   , issueSummary :: IssueSummary
   } deriving (Show, Generic)
