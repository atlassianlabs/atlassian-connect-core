# Atlassian Connect Core (for Haskell)

This is a library that provides Atlassian Connect functionality for applications built on top of the
Snap Framework. We provide a snaplet so that you can integrate Atlassian Connect into your
applications.

## Compiling the code

In order to compile this library we required that you have NixOS installed, in order
to create a reliable GHC 8.01 environment. Run the following to setup a NixOS shell
with GHC:

    $ cat ~/.nixpkgs/config.nix
    {
        packageOverrides = super: let self = super.pkgs; in
        {
            myHaskellEnv =
            self.haskellPackages.ghcWithPackages
                (haskellPackages: with haskellPackages; [
                mtl QuickCheck random text alex cabal-install cpphs happy ghc-paths zlib
                # or anything you like.
                ]);
        };
    }
    $ nix-env -f "<nixpkgs>" -iA myHaskellEnv
    $ nix-shell -p myHaskellEnv

Then run the following to build the library:

    cabal sandbox init
    cabal install # on OSX include the --with-gcc=clang flag

Good luck!
