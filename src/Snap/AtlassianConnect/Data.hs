module Snap.AtlassianConnect.Data
  ( Connect(..)
  , HasConnect(..)
  , HostName
  , DynamicDescriptorConfig(..)
  ) where

-- Connect Modules
import           Data.Connect.Descriptor

-- Standard Modules
import qualified Crypto.Cipher.AES       as CCA
import qualified Data.Text               as T
import qualified Network.URI             as NU

-- | The Atlassian Connect Snaplet Context
data Connect = Connect
  { connectAES              :: CCA.AES      -- ^ The AES key that will be used for Page Token support.
  , connectPageTokenTimeout :: Timeout      -- ^ The Page Token Timeout (how long the token is valid for).
  , connectPlugin           :: Plugin       -- ^ The Atlassian Connect Descriptor for your Add-on.
  , connectBaseUrl          :: NU.URI       -- ^ The URL that your Add-on will report as it's base URL. Especially useful if you are behind a load balancer.
  , connectHostWhitelist    :: [HostName]   -- ^ The suffixes in the hostname that should be allowed to be installed as Connect Add-ons.
  }

-- | Represents a URL hostname
type HostName = T.Text

-- | If a Snap application provides Atlassian Connect then it can impliment HasConnect to allow connect methods to be
-- called easily.
class HasConnect m where
  getConnect :: m Connect

data DynamicDescriptorConfig = DynamicDescriptorConfig
  { dcBaseUrl :: NU.URI
  }
