module Snap.AtlassianConnect.TimeUnits
    ( ConnectSecond(..)
    , getSecond
    ) where

import qualified Data.Configurator.Types as DCT
import           Data.Ratio              (denominator, numerator)
import qualified Data.Time.Units         as DTU

-- | Represents a second for Atlassian Connect
newtype ConnectSecond = CSecond DTU.Second
    deriving(Eq, Show)

-- | Gets the Data.Time.Units representation of this Atlassian Connect second.
getSecond :: ConnectSecond -> DTU.Second
getSecond (CSecond s) = s

instance DCT.Configured ConnectSecond where
  convert (DCT.Number x) = Just . CSecond . fromIntegral . rationalToInteger $ x
  convert _          = Nothing

rationalToInteger :: Rational -> Integer
rationalToInteger x = numerator x `div` denominator x
